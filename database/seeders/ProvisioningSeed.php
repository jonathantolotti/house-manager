<?php

namespace Database\Seeders;

use App\Models\Provisioning;
use Illuminate\Database\Seeder;

class ProvisioningSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provisioning::create([
            'name' => 'Ceee',
            'date' => '2021-03-10',
            'fixed' => 1,
            'type' => 'EXPENSE',
            'user_id' => 1,
            'created_at' => date('Y-m-d H:i:s', strtotime("now")),
            'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
        ]);

        Provisioning::create([
            'name' => 'Fies',
            'date' => '2021-03-05',
            'fixed' => 1,
            'type' => 'EXPENSE',
            'user_id' => 1,
            'created_at' => date('Y-m-d H:i:s', strtotime("now")),
            'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
        ]);

        Provisioning::create([
            'name' => 'Condomínio',
            'date' => '2021-03-10',
            'fixed' => 1,
            'type' => 'EXPENSE',
            'user_id' => 1,
            'created_at' => date('Y-m-d H:i:s', strtotime("now")),
            'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
        ]);

        Provisioning::create([
            'name' => 'NuBank',
            'date' => '2021-03-10',
            'fixed' => 1,
            'type' => 'EXPENSE',
            'user_id' => 1,
            'created_at' => date('Y-m-d H:i:s', strtotime("now")),
            'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
        ]);

        Provisioning::create([
            'name' => 'Plano de Saúde',
            'date' => '2021-03-10',
            'fixed' => 1,
            'type' => 'EXPENSE',
            'user_id' => 1,
            'created_at' => date('Y-m-d H:i:s', strtotime("now")),
            'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
        ]);

        Provisioning::create([
            'name' => 'Faculdade',
            'date' => '2021-03-10',
            'fixed' => 1,
            'type' => 'EXPENSE',
            'user_id' => 1,
            'created_at' => date('Y-m-d H:i:s', strtotime("now")),
            'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
        ]);
    }
}
