<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Jonathan Tolotti',
            'email' => 'jonathanstolotti@gmail.com',
            'password' => Hash::make('1234'),
            'is_admin' => true,
            'income' => 2667.68
        ]);

        User::create([
            'name' => 'Camille Rocha',
            'email' => 'camillerocha10@gmail.com',
            'password' => Hash::make('1234'),
            'is_admin' => false,
            'income' => 2000
        ]);
    }
}
