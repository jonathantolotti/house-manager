<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentForecastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_forecasts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('provisioning_id')->constrained();
            $table->date('due_date');
            $table->dateTime('payment_date')->nullable();
            $table->float('value');
            $table->string('link_invoice')->nullable();
            $table->boolean('paid')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_forecasts');

    }
}
