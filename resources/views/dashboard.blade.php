@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
@stop
@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>

    @if (! $goals)
        <div class="alert alert-info alert-dismissible text-center">
            <h5><i class="icon fas fa-info"></i> Atenção!</h5>
            Você não tem <b>meta</b> cadastradas para este mês, para cadastrar <a href="{{route('goals.new')}}" class="text-gray-dark"><b>clique aqui</b></a>.
        </div>
    @endif

    @if (! $payment_forecasts)
        <div class="alert alert-warning alert-dismissible text-center">
            <h5><i class="icon fas fa-exclamation-triangle"></i> Atenção!</h5>
            Você não tem <b>lançamentos na previsão</b> para este mês, para cadastrar <a href="{{route('payment.forecast.new')}}" class="text-gray-dark"><b>clique aqui</b></a>.
        </div>
    @else

    <div class="row">
        <div class="col-lg-3 col-6 text-center">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>R$ {{number_format($cards_values['entry'], 2, ',', '.')}}</h3>

                    <p>A vencer</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" onclick="detailCard()" class="small-box-footer">Detalhar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6 text-center">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$cards_values['goal_reach']}}<sup style="font-size: 20px">%</sup></h3>

                    <p>Meta atingida</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" onclick="detailCard()" class="small-box-footer">Detalhar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6 text-center">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>R$ {{number_format($cards_values['balance'], 2, ',', '.')}}</h3>

                    <p>Valor em caixa</p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
                <a href="#" onclick="detailCard()" class="small-box-footer">Detalhar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6 text-center">
            <!-- small box -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>R$ {{number_format($cards_values['limit'], 2, ',', '.')}}</h3>

                    <p>Limite de gastos</p>
                </div>
                <div class="icon">
                    <i class="fas fa-ban"></i>
                </div>
                <a href="#" onclick="detailCard()" class="small-box-footer">Detalhar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header border-transparent">
                    <h3 class="card-title">Previsão de <b>{{\App\Enum\Month::getMonth(date("n", strtotime("now")))}}</b></h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="maximize">
                            <i class="fas fa-expand"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive text-center">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Vencimento</th>
                                <th>Status</th>
                                <th>Tipo</th>
                                <th>Data pagamento</th>
                                <th>Valor pago</th>
                                <th>Boleto</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($latest_forecasts as $last_forecast)
                                <tr>
                                    <td>
                                        <a class="font-weight-bold" href="{{route('payment.forecast.edit', $last_forecast['id'])}}">{{$last_forecast['name']}}</a>
                                    </td>
                                    <td>
                                        {{date("d/m/Y", strtotime($last_forecast['due_date']))}}
                                    </td>
                                    <td>
                                    @if($last_forecast['paid'])
                                            <span class="badge alert-success">Pago</span>
                                    @elseif(! $last_forecast['paid'] && strtotime(date('Y-m-d')) == strtotime($last_forecast['due_date']))
                                            <button class="btn btn-sm badge alert-warning" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                    onclick="payForecast(
                                                        '{{$last_forecast['name']}}',
                                                        '{{date("d/m/Y", strtotime($last_forecast['due_date']))}}',
                                                        '{{number_format($last_forecast['value'], 2, ',', '.')}}',
                                                        '{{$last_forecast['id']}}',
                                                        '{{route('payment.forecast.pay')}}'
                                                        )">Pagar
                                            </button>
                                    @elseif(! $last_forecast['paid'] && strtotime(date('Y-m-d')) > strtotime($last_forecast['due_date']))
                                            <button class="btn btn-sm badge alert-danger" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                    onclick="payForecast(
                                                        '{{$last_forecast['name']}}',
                                                        '{{date("d/m/Y", strtotime($last_forecast['due_date']))}}',
                                                        '{{number_format($last_forecast['value'], 2, ',', '.')}}',
                                                        '{{$last_forecast['id']}}',
                                                        '{{route('payment.forecast.pay')}}'
                                                        )">Vencido
                                            </button>
                                        @elseif(! $last_forecast['paid'] && strtotime(date('Y-m-d')) < strtotime($last_forecast['due_date']))
                                            <button class="btn btn-sm badge alert-info" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                    onclick="payForecast(
                                                        '{{$last_forecast['name']}}',
                                                        '{{date("d/m/Y", strtotime($last_forecast['due_date']))}}',
                                                        '{{number_format($last_forecast['value'], 2, ',', '.')}}',
                                                        '{{$last_forecast['id']}}',
                                                        '{{route('payment.forecast.pay')}}'
                                                        )">Pendente
                                            </button>
                                        @endif
                                    </td>

                                    <td>
                                        <span class="badge badge-{{$last_forecast['type'] === "INCOME" ? "success" : "danger"}}">{{\App\Enum\Provisioning::getType($last_forecast['type'])}}</span>
                                    </td>

                                    <td>
                                        @if($last_forecast['payment_date'])
                                            {{date("d/m/Y H:i:s", strtotime($last_forecast['payment_date']))}}
                                        @else
                                            <span class="badge alert-info">Pendente</span>
                                        @endif
                                    </td>
                                    <td>
                                        R$ {{number_format($last_forecast['value'], 2, ',', '.')}}
                                    </td>
                                    <td>
                                        @if($last_forecast['link_invoice'])
                                            <a href="{{asset(Storage::url($last_forecast['link_invoice']))}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Clique para visualizar o boleto">
                                                <i class="fas fa-file-invoice"></i>
                                            </a>
                                        @else
                                            <i class="fas fa-minus-circle" data-toggle="tooltip" data-placement="top" title="Não há boleto disponível"></i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" name="_token" id="_token" value="{{@csrf_token()}}">
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <a href="{{route('payment.forecast.new')}}" class="btn btn-sm btn-info float-left">Nova previsão</a>
                    <a href="{{route('payment.forecast.index')}}" class="btn btn-sm btn-secondary float-right">Ver todos</a>
                </div>
                <!-- /.card-footer -->
            </div>
            <div class="card collapsed-card">
                <div class="card-header border-transparent">
                    <h3 class="card-title">Previsão de <b>{{\App\Enum\Month::getMonth(date("n", strtotime("now +1 month")))}}</b></h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="maximize">
                            <i class="fas fa-expand"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive text-center">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Vencimento</th>
                                <th>Status</th>
                                <th>Tipo</th>
                                <th>Data pagamento</th>
                                <th>Valor pago</th>
                                <th>Boleto</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($sum = 0)
                            @foreach($next_month_forecasts as $last_forecast)
                                @php($sum+=$last_forecast['value'])
                                <tr>
                                    <td>
                                        <a href="{{route('payment.forecast.edit', $last_forecast['id'])}}">{{$last_forecast['name']}}</a>
                                    </td>
                                    <td>
                                        {{date("d/m/Y", strtotime($last_forecast['due_date']))}}
                                    </td>
                                    <td>
                                        @if($last_forecast['paid'])
                                            <span class="badge alert-success">Pago</span>
                                        @elseif(! $last_forecast['paid'] && strtotime(date('Y-m-d')) == strtotime($last_forecast['due_date']))
                                            <button class="btn btn-sm badge alert-warning" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                    onclick="payForecast(
                                                        '{{$last_forecast['name']}}',
                                                        '{{date("d/m/Y", strtotime($last_forecast['due_date']))}}',
                                                        '{{number_format($last_forecast['value'], 2, ',', '.')}}',
                                                        '{{$last_forecast['id']}}',
                                                        '{{route('payment.forecast.pay')}}'
                                                        )">Pagar
                                            </button>
                                        @elseif(! $last_forecast['paid'] && strtotime(date('Y-m-d')) > strtotime($last_forecast['due_date']))
                                            <button class="btn btn-sm badge alert-danger" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                    onclick="payForecast(
                                                        '{{$last_forecast['name']}}',
                                                        '{{date("d/m/Y", strtotime($last_forecast['due_date']))}}',
                                                        '{{number_format($last_forecast['value'], 2, ',', '.')}}',
                                                        '{{$last_forecast['id']}}',
                                                        '{{route('payment.forecast.pay')}}'
                                                        )">Vencido
                                            </button>
                                        @elseif(! $last_forecast['paid'] && strtotime(date('Y-m-d')) < strtotime($last_forecast['due_date']))
                                            <button class="btn btn-sm badge alert-info" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                    onclick="payForecast(
                                                        '{{$last_forecast['name']}}',
                                                        '{{date("d/m/Y", strtotime($last_forecast['due_date']))}}',
                                                        '{{number_format($last_forecast['value'], 2, ',', '.')}}',
                                                        '{{$last_forecast['id']}}',
                                                        '{{route('payment.forecast.pay')}}'
                                                        )">Pendente
                                            </button>
                                        @endif
                                    </td>

                                    <td>
                                        <span class="badge badge-{{$last_forecast['type'] === "INCOME" ? "success" : "danger"}}">{{\App\Enum\Provisioning::getType($last_forecast['type'])}}</span>
                                    </td>

                                    <td>
                                        @if($last_forecast['payment_date'])
                                            {{date("d/m/Y H:i:s", strtotime($last_forecast['payment_date']))}}
                                        @else
                                            <span class="badge alert-info">Pendente</span>
                                        @endif
                                    </td>
                                    <td>
                                        R$ {{number_format($last_forecast['value'], 2, ',', '.')}}
                                    </td>
                                    <td>
                                        @if($last_forecast['link_invoice'])
                                            <a href="{{asset(Storage::url($last_forecast['link_invoice']))}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Clique para visualizar o boleto">
                                                <i class="fas fa-file-invoice"></i>
                                            </a>
                                        @else
                                            <i class="fas fa-minus-circle" data-toggle="tooltip" data-placement="top" title="Não há boleto disponível"></i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" name="_token" id="_token" value="{{@csrf_token()}}">
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center clearfix">
                    <a href="{{route('payment.forecast.new')}}" class="btn btn-sm btn-info float-left">Nova previsão</a>
                    <span>Valor total: <b>R$ {{number_format($sum, 2, ',', '.')}}</b></span>
                    <a href="{{route('payment.forecast.index')}}" class="btn btn-sm btn-secondary float-right">Ver todos</a>
                </div>
                <!-- /.card-footer -->
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header border-transparent">
                    <h3 class="card-title">Últimas movimentações</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="maximize">
                            <i class="fas fa-expand"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table m-0 text-center">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Vencimento</th>
                                <th>Fixo</th>
                                <th>Tipo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($latest_entry as $last_entry)
                                <tr>
                                    <td><a href="{{route('provisioning.edit', $last_entry['id'])}}">{{$last_entry['name']}}</a></td>
                                    <td>{{$last_entry['date']}}</td>
                                    <td>
                                        <i class="fas fa-thumbs-{{$last_entry['fixed'] ? "up" : "down"}}"></i>
                                    </td>
                                    <td>
                                        <span class="badge badge-{{$last_entry['type'] === "Receita" ? "success" : "danger"}}">{{$last_entry['type']}}</span>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <a href="{{route('provisioning.new')}}" class="btn btn-sm btn-info float-left">Nova conta</a>
                    <a href="{{route('provisioning.index')}}" class="btn btn-sm btn-secondary float-right">Ver todos</a>
                </div>
                <!-- /.card-footer -->
            </div>
        </div>

    </div>
    @endif

@stop

@section('js')
    <script src="{{asset('/js/pages/Dashboard/dashboard.js')}}"></script>
@stop
