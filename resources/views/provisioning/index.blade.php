@extends('adminlte::page')
@section('title', 'Contas')

@if (session('success'))
    <script>
        window.onload = function() {
            Swal.fire({
                position: 'top-end',
                icon: "success",
                title: "{{session('success')}}",
                showConfirmButton: false,
                timer: 1500
            })
            setInterval(()=>{
                location.reload();
            }, 1500)
        };
    </script>

@endif
@if (session('error'))
    <script>
        window.onload = function() {
            Swal.fire({
                position: 'top-end',
                icon: "error",
                title: "{{session('error')}}",
                showConfirmButton: false,
                timer: 1500
            })
            setInterval(()=>{
                location.reload();
            }, 1500)
        };
    </script>
@endif

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item active">Contas</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Listagem de contas</h3>

                    <div class="card-tools">
                        <div class="input-group">

                            <div class="input-group-append">
                                <a href="{{route('provisioning.new')}}" class="btn btn-success"><i class="fas fa-plus"></i> Adicionar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table id="provisioning" class="table table-bordered table-hover text-center">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Vencimento</th>
                            <th>Fixa</th>
                            <th>Tipo</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($provisions as $provision)
                            <tr>
                                <td>{{$provision['name']}}</td>
                                <td>{{$provision['date']}}</td>
                                <td>
                                    <i class="fas fa-thumbs-{{$provision['fixed'] ? "up" : "down"}}"></i>
                                </td>
                                <td>{{$provision['type']}}</td>
                                <td>
                                    <a href="{{route('provisioning.edit', $provision['id'])}}" class="btn btn-info">Editar</a>
                                    <span data-toggle="tooltip" data-placement="top"
                                          title="{{$provision['has_forecast'] ? "Já lançado neste mês" : "Clique para criar uma previsão para este lançamento"}}">
                                        <a href="{{route('payment.forecast.new', $provision['id'])}}"
                                           class="btn {{$provision['has_forecast'] ? "btn-warning" : "btn-success"}}">
                                            <i class='icon fas fa-exclamation-triangle {{$provision['has_forecast'] ? "" : "d-none"}}'></i>
                                            Previsão</a>
                                    </span>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Vencimento</th>
                            <th>Fixa</th>
                            <th>Tipo</th>
                            <th>Ações</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <input type="hidden" name="_token" id="_token" value="{{@csrf_token()}}">

@stop

@section('js')
    <script src="{{asset('/js/pages/Provisioning/provisioning.js')}}"></script>
@stop
