@extends('adminlte::page')
@section('title', 'Alterar conta')
@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('provisioning.index')}}">Contas</a></li>
                    <li class="breadcrumb-item active">Editar conta</li>
                </ol>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row"> <!-- MODELO 3 -->
                <div class="container">
                    <div class="row">
                        <div class="col align-self-start"></div>
                        <div class="col-md-12">
                            <div class="col align-self-center">
                                <div class="card card-dark">
                                    <div class="card-header">
                                        <h3 class="card-title">Nova meta</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form method="POST" action="{{ route('provisioning.update', $provisioning['id']) }}" role="form">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <!-- NOME -->
                                                <div class="input-group mb-3">
                                                    <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                           value="{{ $provisioning['name'] }}" placeholder="Nome da provisão" autofocus>
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-file-signature {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('name'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="input-group mb-3">
                                                    <input type="date" name="date" class="form-control value {{ $errors->has('date') ? 'is-invalid' : '' }}"
                                                           value="{{ $provisioning['date'] }}" placeholder="Vencimento">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-calendar {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('date'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('date') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="input-group mb-3">
                                                    <select name="type" class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}">
                                                        <option value="INCOME" {{$provisioning['type'] == "INCOME" ? "selected" : ""}}>Receita</option>
                                                        <option value="EXPENSE" {{$provisioning['type'] == "EXPENSE" ? "selected" : ""}}>Despesa</option>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-clipboard-check {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('type'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('type') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <label for="exampleInputPassword1">Lançamento fixo:</label>
                                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                    <input name="fixed" type="checkbox" class="custom-control-input" id="customSwitch1" {{$provisioning['fixed'] ? "checked" : ""}}>
                                                    <label class="custom-control-label" for="customSwitch1"></label>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <div class="row justify-content-end">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
                                                        <span class="fas fa-user-plus"></span>
                                                        Cadastrar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col align-self-end"></div>
                    </div>
                </div>
            </div> <!-- FIM MODELO 3 -->
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@stop
