@extends('adminlte::page')
@section('title', 'Nova previsão')
@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('payment.forecast.index')}}">Previsão</a></li>
                    <li class="breadcrumb-item active">Nova previsão</li>
                </ol>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row"> <!-- MODELO 3 -->
                <div class="container">
                    <div class="row">
                        <div class="col align-self-start"></div>
                        <div class="col-md-12">
                            <div class="col align-self-center">
                                <div class="card card-dark">
                                    <div class="card-header">
                                        <h3 class="card-title">Nova previsão</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form method="POST" action="{{ route('payment.forecast.store') }}" role="form" enctype="multipart/form-data">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <!-- NOME -->
                                                <div class="input-group mb-3">
                                                    <select name="provisioning" class="form-control select2 {{ $errors->has('provisioning_name') ? 'is-invalid' : '' }}">
                                                        <option selected disabled>Selecione</option>
                                                        @foreach($allProvisioning as $provisioning)
                                                            <option value="{{$provisioning['id']}}" {{isset($currentProvisioning['id']) && $currentProvisioning['id'] == $provisioning['id'] ? "selected" : ""}}>{{$provisioning['name']}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-clipboard-check {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('provisioning_name'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('provisioning_name') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="input-group mb-3">
                                                    <input type="date" name="due_date" class="form-control {{ $errors->has('due_date') ? 'is-invalid' : '' }}"
                                                           value="{{ isset($currentProvisioning->date) ? date('Y-m-d', strtotime($currentProvisioning->date)) : ""}}" placeholder="Vencimento">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-calendar {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('due_date'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('due_date') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="input-group mb-3">
                                                    <input type="text" name="value" class="form-control value {{ $errors->has('value') ? 'is-invalid' : '' }}"
                                                           value="{{ old('value') }}" placeholder="Valor do lançamento">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-money-bill {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('value'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('value') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="form-group">

                                                    <div class="custom-file">
                                                        <input type="file" name="invoice" class="custom-file-input" id="customFile">
                                                        <label class="custom-file-label" for="customFile" content="Procurar">Selecione o boleto bancário...</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <div class="row justify-content-end">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
                                                        <span class="fas fa-user-plus"></span>
                                                        Cadastrar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col align-self-end"></div>
                    </div>
                </div>
            </div> <!-- FIM MODELO 3 -->
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@stop

@section('js')
    <script src="{{asset('/js/helpers/jquery.mask.js')}}"></script>
    <script src="{{asset('/js/pages/Forecasts/forecasts.js')}}"></script>
@stop
