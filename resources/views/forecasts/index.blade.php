@extends('adminlte::page')
@section('title', 'Previsão')

@if (session('success'))
    <script>
        window.onload = function() {
            Swal.fire({
                position: 'top-end',
                icon: "success",
                title: "{{session('success')}}",
                showConfirmButton: false,
                timer: 1500
            })
            setInterval(()=>{
                location.reload();
            }, 1500)
        };
    </script>

@endif
@if (session('error'))
    <script>
        window.onload = function() {
            Swal.fire({
                position: 'top-end',
                icon: "error",
                title: "{{session('error')}}",
                showConfirmButton: false,
                timer: 1500
            })
            setInterval(()=>{
                location.reload();
            }, 1500)
        };
    </script>
@endif
@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item active">Previsão</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Lançamentos</h3>

                    <div class="card-tools">
                        <div class="input-group">

                            <div class="input-group-append">
                                <a href="{{route('payment.forecast.new')}}" class="btn btn-success"><i class="fas fa-plus"></i> Adicionar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table id="goals" class="table table-bordered table-hover text-center">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Vencimento</th>
                            <th>Status</th>
                            <th>Data pagamento</th>
                            <th>Valor</th>
                            <th>Boleto</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($forecasts as $forecast)
                            <tr>
                                <td>
                                    {{$forecast['name']}}
                                </td>
                                <td>
                                    {{date("d/m/Y", strtotime($forecast['due_date']))}}
                                </td>
                                <td>
                                    @if($forecast['paid'])
                                        <span class="badge alert-success">Pago</span>
                                    @elseif(! $forecast['paid'] && strtotime(date('Y-m-d')) == strtotime($forecast['due_date']))
                                        <button class="btn btn-sm badge alert-warning" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                onclick="payForecast(
                                                    '{{$forecast['name']}}',
                                                    '{{date("d/m/Y", strtotime($forecast['due_date']))}}',
                                                    '{{number_format($forecast['value'], 2, ',', '.')}}',
                                                    '{{$forecast['id']}}',
                                                    '{{route('payment.forecast.pay')}}'
                                                    )">Pagar
                                        </button>
                                    @elseif(! $forecast['paid'] && strtotime(date('Y-m-d')) > strtotime($forecast['due_date']))
                                        <button class="btn btn-sm badge alert-danger" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                onclick="payForecast(
                                                    '{{$forecast['name']}}',
                                                    '{{date("d/m/Y", strtotime($forecast['due_date']))}}',
                                                    '{{number_format($forecast['value'], 2, ',', '.')}}',
                                                    '{{$forecast['id']}}',
                                                    '{{route('payment.forecast.pay')}}'
                                                    )">Vencido
                                        </button>
                                    @elseif(! $forecast['paid'] && strtotime(date('Y-m-d')) < strtotime($forecast['due_date']))
                                        <button class="btn btn-sm badge alert-info" data-toggle="tooltip" data-placement="top" title="Clique para pagar"
                                                onclick="payForecast(
                                                    '{{$forecast['name']}}',
                                                    '{{date("d/m/Y", strtotime($forecast['due_date']))}}',
                                                    '{{number_format($forecast['value'], 2, ',', '.')}}',
                                                    '{{$forecast['id']}}',
                                                    '{{route('payment.forecast.pay')}}'
                                                    )">Pendente
                                        </button>
                                    @endif

                                </td>
                                <td>
                                    @if($forecast['payment_date'])
                                        {{date("d/m/Y H:i:s", strtotime($forecast['payment_date']))}}
                                    @else
                                        <span class="badge alert-info">Pendente</span>
                                    @endif
                                </td>
                                <td>
                                    R$ {{number_format($forecast['value'], 2, ',', '.')}}
                                </td>
                                <td>
                                    @if($forecast['link_invoice'])
                                        <a href="{{asset(Storage::url($forecast['link_invoice']))}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Clique para visualizar o boleto">
                                            <i class="fas fa-file-invoice"></i>
                                        </a>
                                    @else
                                        <i class="fas fa-minus-circle" data-toggle="tooltip" data-placement="top" title="Não há boleto disponível"></i>
                                    @endif
                                </td>
                                <td>
                                   <a href="{{route('payment.forecast.edit', $forecast['id'])}}" class="btn btn-info">Editar</a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Vencimento</th>
                            <th>Status</th>
                            <th>Data pagamento</th>
                            <th>Valor</th>
                            <th>Boleto</th>
                            <th>Ações</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <input type="hidden" name="_token" id="_token" value="{{@csrf_token()}}">
@stop

@section('js')
    <script src="{{asset('/js/pages/Forecasts/forecasts.js')}}"></script>
@stop


