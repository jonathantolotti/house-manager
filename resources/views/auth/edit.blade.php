@extends('adminlte::page')
@section('title', 'Registro')

@if (session('success'))
    <script>
        window.onload = function() {
            Swal.fire({
                position: 'top-end',
                icon: "success",
                title: "{{session('success')}}",
                showConfirmButton: false,
                timer: 1500
            })
            setInterval(()=>{
                location.reload();
            }, 1500)
        };
    </script>

@endif
@if (session('error'))
    <script>
        window.onload = function() {
            Swal.fire({
                position: 'top-end',
                icon: "error",
                title: "{{session('error')}}",
                showConfirmButton: false,
                timer: 1500
            })
            setInterval(()=>{
                location.reload();
            }, 1500)
        };
    </script>
@endif
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row"> <!-- MODELO 3 -->
                <div class="container">
                    <div class="row">
                        <div class="col align-self-start"></div>
                        <div class="col-md-12">
                            <div class="col align-self-center">
                                <div class="card card-dark">
                                    <div class="card-header">
                                        <h3 class="card-title">Meu perfil</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form method="POST" action="{{ route('users.update') }}" role="form">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <!-- NOME -->
                                                <div class="input-group mb-3">
                                                    <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                           value="{{ $user->name }}" placeholder="{{ __('adminlte::adminlte.full_name') }}" autofocus>
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-user {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('name'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="input-group mb-3">
                                                    <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                                           value="{{ $user->email }}" placeholder="{{ __('adminlte::adminlte.email') }}" readonly>
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('email'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="input-group mb-3">
                                                    <input type="text" name="income" class="form-control income {{ $errors->has('income') ? 'is-invalid' : '' }}"
                                                           value="{{ number_format($user->income, 2, ',', '.') }}" placeholder="R$">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-money-bill {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('income'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('income') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <div class="row justify-content-end">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
                                                        <span class="fas fa-user-plus"></span>
                                                        Atualizar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col align-self-end"></div>
                    </div>
                </div>
            </div> <!-- FIM MODELO 3 -->
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    @stop

@section('js')
    <script src="{{asset('/js/helpers/jquery.mask.js')}}"></script>
    <script src="{{asset('/js/pages/User/user.js')}}"></script>
@stop


