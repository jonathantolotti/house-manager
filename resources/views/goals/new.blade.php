@extends('adminlte::page')
@section('title', 'Nova meta')

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('goals.index')}}">Metas</a></li>
                    <li class="breadcrumb-item active">Nova meta</li>
                </ol>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row"> <!-- MODELO 3 -->
                <div class="container">
                    <div class="row">
                        <div class="col align-self-start"></div>
                        <div class="col-md-12">
                            <div class="col align-self-center">
                                <div class="card card-dark">
                                    <div class="card-header">
                                        <h3 class="card-title">Nova meta</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form method="POST" action="{{ route('goals.store') }}" role="form">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <!-- NOME -->
                                                <div class="input-group mb-3">
                                                    <input type="date" name="start_date" class="form-control {{ $errors->has('start_date') ? 'is-invalid' : '' }}"
                                                           value="{{ old('start_date') }}" autofocus>
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-calendar {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('start_date'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('start_date') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="input-group mb-3">
                                                    <input type="text" name="value" class="form-control value {{ $errors->has('value') ? 'is-invalid' : '' }}"
                                                           value="{{ old('value') }}" placeholder="Valor da meta">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <span class="fas fa-money-bill {{ config('adminlte.classes_auth_icon', '') }}"></span>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('value'))
                                                        <div class="invalid-feedback">
                                                            <strong>{{ $errors->first('value') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <div class="row justify-content-end">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
                                                        <span class="fas fa-user-plus"></span>
                                                        {{ __('adminlte::adminlte.register') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col align-self-end"></div>
                    </div>
                </div>
            </div> <!-- FIM MODELO 3 -->
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('js')
    <script src="{{asset('/js/helpers/jquery.mask.js')}}"></script>
    <script src="{{asset('/js/pages/Goals/goals.js')}}"></script>
@stop
