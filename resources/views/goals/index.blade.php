@extends('adminlte::page')
@section('title', 'Metas')

@if (session('success'))
    <script>
        window.onload = function() {
            Swal.fire({
                position: 'top-end',
                icon: "success",
                title: "{{session('success')}}",
                showConfirmButton: false,
                timer: 1500
            })
            setInterval(()=>{
                location.reload();
            }, 1500)
        };
    </script>

@endif
@if (session('error'))
    <script>
        window.onload = function() {
            Swal.fire({
                position: 'top-end',
                icon: "error",
                title: "{{session('error')}}",
                showConfirmButton: false,
                timer: 1500
            })
            setInterval(()=>{
                location.reload();
            }, 1500)
        };
    </script>
@endif
@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item active">Metas</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Listagem de metas</h3>

                    <div class="card-tools">
                        <div class="input-group">

                            <div class="input-group-append">
                                <a href="{{route('goals.new')}}" class="btn btn-success"><i class="fas fa-plus"></i> Adicionar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table id="goals" class="table table-bordered table-hover text-center">
                        <thead>
                        <tr>
                            <th>Mês</th>
                            <th>Valor</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($goals as $goal)
                            <tr>
                                <td>{{$goal['start_date']}}</td>
                                <td>{{number_format($goal['value'], 2, ',', '.')}}</td>
                                <td>
                                    <i class="fas fa-thumbs-{{$goal['reach'] ? "up" : "down"}}"></i>
                                </td>
                                <td>
                                    <a href="{{route('goals.edit', $goal['id'])}}" class="btn btn-info btn-sm">Editar</a>
                                    @if (! $goal['reach'])
                                        <a href="{{route('goals.finalize', $goal['id'])}}" class="btn btn-success btn-sm">Finalizar</a>
                                    @else
                                        <a href="{{route('goals.reopen', $goal['id'])}}" class="btn btn-warning btn-sm">Reabrir</a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Mês</th>
                            <th>Valor</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <input type="hidden" name="_token" id="_token" value="{{@csrf_token()}}">
@stop

@section('js')
    <script src="{{asset('/js/pages/Goals/goals.js')}}"></script>
@stop
