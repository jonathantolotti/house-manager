<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GoalsController;
use App\Http\Controllers\PaymentForecastController;
use App\Http\Controllers\ProvisioningController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/', function () {
        return redirect('/dashboard');
    });

    /**
     * Rotas referente usuários
     */
    Route::prefix('users')->group(function (){
        Route::name('users.')->group(function () {
            Route::get('new', [UserController::class, 'register'])->name('new');
            Route::post('store', [UserController::class, 'store'])->name('store');
            Route::get('edit', [UserController::class, 'edit'])->name('edit');
            Route::post('update', [UserController::class, 'update'])->name('update');
        });
    });

    /**
     * Rotas para metas
     */
    Route::prefix('goals')->group(function (){
        Route::name('goals.')->group(function () {
            Route::get('', [GoalsController::class, 'index'])->name('index');
            Route::get('new', [GoalsController::class, 'new'])->name('new');
            Route::post('store', [GoalsController::class, 'store'])->name('store');
            Route::get('edit/{id}', [GoalsController::class, 'edit'])->name('edit');
            Route::post('update/{id}', [GoalsController::class, 'update'])->name('update');
            Route::get('finalize/{id}', [GoalsController::class, 'finalize'])->name('finalize');
            Route::get('reopen/{id}', [GoalsController::class, 'reopen'])->name('reopen');
        });
    });

    /**
     * Rotas para previsão
     */
    Route::prefix('payment-forecast')->group(function (){
        Route::name('payment.forecast.')->group(function () {
            Route::get('', [PaymentForecastController::class, 'index'])->name('index');
            Route::get('new/{provisioningId?}', [PaymentForecastController::class, 'new'])->name('new');
            Route::post('store', [PaymentForecastController::class, 'store'])->name('store');
            Route::get('edit/{id}', [PaymentForecastController::class, 'edit'])->name('edit');
            Route::post('update/{id}', [PaymentForecastController::class, 'update'])->name('update');
            Route::post('pay', [PaymentForecastController::class, 'pay'])->name('pay');
        });
    });

    /**
     * Rotas para provisão
     */
    Route::prefix('provisioning')->group(function (){
        Route::name('provisioning.')->group(function () {
            Route::get('', [ProvisioningController::class, 'index'])->name('index');
            Route::get('new', [ProvisioningController::class, 'new'])->name('new');
            Route::post('store', [ProvisioningController::class, 'store'])->name('store');
            Route::get('edit/{id}', [ProvisioningController::class, 'edit'])->name('edit');
            Route::post('update/{id}', [ProvisioningController::class, 'update'])->name('update');
        });
    });


});

Auth::routes();
