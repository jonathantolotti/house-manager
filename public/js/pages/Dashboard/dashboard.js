$(function () {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
});

function payForecast(name, due_date, value, forecastId, route)
{
    Swal.fire({
        title: 'Confira os dados antes de efetuar o pagamento:',
        html: `<b>Nome:</b> ${name}<br>` +
              `<b>Vencimento:</b> ${due_date}<br>` +
              `<b>Valor:</b> R$ ${value}<br>`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: route,
                type: 'POST',
                dataType: "json",
                data: {
                    forecastId: forecastId,
                    _token: $("#_token").val()
                },
                success: (response) => {
                    console.log(response);
                    Swal.fire({
                        position: 'top-end',
                        icon: response.success ? "success" : "error",
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                    setInterval(()=>{
                        location.reload();
                    }, 1500)
                }
            });
        }
    })
}

function detailCard()
{
    Swal.fire({
        position: 'top-center',
        icon: 'info',
        title: 'Em breve...',
        showConfirmButton: false,
        timer: 1500
    })
}
