$(function () {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('#provisioning').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": false,
        "responsive": true,
        language: {
            "emptyTable": "Nenhum registro encontrado",
            "paginate": {
                "next": "Próximo",
                "previous": "Anterior",
                "first": "Primeiro",
                "last": "Último"
            }
        }
    });
});
