<?php


namespace App\Http\Requests\Goals;


use App\Http\Requests\Base;

class Store extends Base
{

    protected function prepareForValidation()
    {
        $this->merge([
            'value' => $this->unmaskPrice('value'),
            'end_date' => date("Y-m-t", strtotime("now")),
            'user_id' => auth()->id()
        ]);
    }

    public function rules(): array
    {
        return [
            'start_date' => ['date']
        ];
    }
}
