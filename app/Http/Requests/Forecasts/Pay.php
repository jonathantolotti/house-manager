<?php


namespace App\Http\Requests\Forecasts;


use App\Http\Requests\Base;

/***
 * Class Pay
 *
 * @package App\Http\Requests\Forecasts
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class Pay extends Base
{
    protected function prepareForValidation()
    {
        $this->merge([
            '_token' => $this->input('_token'),
            'user_id' => auth()->id()
        ]);
    }

    public function rules(): array
    {
        return [
            'forecastId' => ['required', 'numeric']
        ];
    }
}
