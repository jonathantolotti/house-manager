<?php


namespace App\Http\Requests\Forecasts;


use App\Http\Requests\Base;

class Store extends Base
{
    protected function prepareForValidation()
    {
        $this->merge([
            'value' => $this->unmaskPrice('value')
        ]);
    }

    public function rules(): array
    {
        return [
            'provisioning' => ['required', 'numeric'],
            'due_date' => ['required', 'date'],
            'value' => ['required', 'numeric'],
            'file' => ['file', 'mimes:jpg,png,jpeg,pdf']
        ];
    }
}
