<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Base;
use Illuminate\Support\Facades\Hash;

/***
 * Class Store
 *
 * @package App\Http\Requests\Auth
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class Store extends Base
{
    protected function prepareForValidation()
    {
        $this->merge([
            'is_admin' => $this->input('is_admin') == 'on',
            'password' => Hash::make($this->input('password')),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:4']
        ];
    }

}
