<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Base;

/***
 * Class Store
 *
 * @package App\Http\Requests\Auth
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class Update extends Base
{
    protected function prepareForValidation()
    {
        $this->merge([
            'income' => $this->unmaskPrice('income')
        ]);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'income' => ['required', 'numeric']
        ];
    }

}
