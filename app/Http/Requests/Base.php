<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/***
 * Class Base
 *
 * @package App\Http\Requests
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

abstract class Base extends FormRequest
{

    /**
     * Retorna as regras que serão aplicadas em cada campo do formulário.
     *
     * @return array
     */
    abstract public function rules(): array;

    /**
     * Retorna o valor sem a formatação da máscara.
     *
     * @param string $fieldName
     *
     * @return float
     */
    protected function unmaskPrice(string $fieldName): float
    {
        return (float) str_replace(',', '.', str_replace('.', '', $this->input($fieldName)));
    }

}
