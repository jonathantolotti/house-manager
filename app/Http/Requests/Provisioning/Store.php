<?php


namespace App\Http\Requests\Provisioning;


use App\Http\Requests\Base;
use Illuminate\Validation\Rule;

class Store extends Base
{
    protected function prepareForValidation()
    {
        $this->merge([
            'user_id' => auth()->id(),
            'fixed' => $this->input('fixed') == 'on',
        ]);
    }

    public function rules(): array
    {
        return [
            'name' => ['required'],
            'date' => ['date'],
            'type' => ['required', Rule::in(['INCOME', 'EXPENSE'])]
        ];
    }
}
