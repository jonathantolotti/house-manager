<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\Store;
use App\Http\Requests\Auth\Update;
use App\Models\User;
use App\Repository\UserRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Retorna view de registro
     *
     * @return Application|Factory|View
     */
    public function register()
    {
        return view('auth.register');
    }

    /**
     * @param Store $request
     * @return RedirectResponse
     */
    public function store(Store $request): RedirectResponse
    {
        return UserRepository::store($request);
    }

    /**
     * @return Application|Factory|View
     */
    public function edit()
    {
        return view('auth.edit', [
            'user' => auth()->user()
        ]);
    }

    public function update(Update $request)
    {
        return UserRepository::update($request);
    }
}
