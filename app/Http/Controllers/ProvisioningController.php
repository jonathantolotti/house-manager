<?php

namespace App\Http\Controllers;

use App\Http\Requests\Provisioning\Store;
use App\Repository\ProvisioningRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/***
 * Class ProvisioningController
 *
 * @package App\Http\Controllers
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class ProvisioningController extends Controller
{
    /**
     * Retorna listagem de provisões.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('provisioning.index', [
            'provisions' => ProvisioningRepository::getAll()
        ]);
    }

    /**
     * Retorna view de criação de provisão.
     *
     * @return Application|Factory|View
     */
    public function new()
    {
        return view('provisioning.new');
    }

    /**
     * Cria uma nova provisão
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public function store(Store $request): RedirectResponse
    {
        return ProvisioningRepository::store($request);
    }

    /**
     * View de edição da provisão
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id)
    {
        return view('provisioning.edit', [
            'provisioning' => ProvisioningRepository::edit($id)
        ]);
    }

    /**
     * Altera uma provisão
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public function update(Store $request): RedirectResponse
    {
        return ProvisioningRepository::update($request);
    }
}
