<?php

namespace App\Http\Controllers;

use App\Http\Requests\Forecasts\Pay;
use App\Http\Requests\Forecasts\Store;
use App\Http\Requests\Forecasts\Update;
use App\Repository\PaymentForecastRepository;
use App\Repository\ProvisioningRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

/***
 * Class PaymentForecastController
 *
 * @package App\Http\Controllers
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class PaymentForecastController extends Controller
{
    /**
     * Retorna listagem de previsão
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $forecasts = PaymentForecastRepository::getAll();

        return view('forecasts.index', [
            'forecasts' => $forecasts
        ]);
    }

    /**
     * Retorna a view para criação de uma nova previsão.
     *
     * @param int|null $provisioningId
     * @return Application|Factory|View
     */
    public function new(int $provisioningId = null)
    {
        $provisioning = [];

        if ($provisioningId) {
            $provisioning = ProvisioningRepository::edit($provisioningId);
        }

        return view('forecasts.new', [
            'currentProvisioning' => $provisioning,
            'allProvisioning' => ProvisioningRepository::getAll()
        ]);
    }

    /**
     * Cria uma nova previsão
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public function store(Store $request): RedirectResponse
    {
        return PaymentForecastRepository::store($request);
    }

    /**
     * Carrega view de edição.
     *
     * @param int $forecastId
     * @return Application|Factory|View
     */
    public function edit(int $forecastId)
    {
        $forecast = PaymentForecastRepository::edit($forecastId);

        return view('forecasts.edit', [
            'forecast' => $forecast
        ]);
    }

    /**
     * Altera uma previsão
     *
     * @param Update $request
     * @return RedirectResponse
     */
    public function update(Update $request): RedirectResponse
    {
        return PaymentForecastRepository::update($request);
    }

    /**
     * Efetua o pagamento de uma previsão
     *
     * @param Pay $request
     * @return false|string
     */
    public function pay(Pay $request)
    {
        if (PaymentForecastRepository::pay($request)) {
            return json_encode([
                'success' => true,
                'message' => 'Previsão paga com sucesso'
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => 'Erro ao efetuar o pagamento'
        ]);
    }
}
