<?php

namespace App\Http\Controllers;

use App\Http\Requests\Goals\Store;
use App\Models\UserGoal;
use App\Repository\GoalRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class GoalsController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $goals = GoalRepository::getAllGoals();

        return view('goals.index', [
            'goals' => $goals
        ]);
    }

    /**
     * Retorna view de criação de metas
     *
     * @return Application|Factory|View
     */
    public function new()
    {
        return view('goals.new');
    }

    /**
     * @param Store $request
     * @return RedirectResponse
     */
    public function store(Store $request): RedirectResponse
    {
        return GoalRepository::createGoal($request);
    }

    /**
     * Retorna view de edição da meta
     *
     * @param int $goalId
     * @return Application|Factory|View
     */
    public function edit(int $goalId)
    {
        $goal = GoalRepository::getGoal($goalId);

        return view('goals.edit', ['goal' => $goal]);
    }

    /**
     * Altera dados de uma meta
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public function update(Store $request): RedirectResponse
    {
        return GoalRepository::updateGoal($request);
    }

    /**
     * Finaliza uma entrega
     *
     * @param int $goalId
     * @return RedirectResponse
     */
    public function finalize(int $goalId): RedirectResponse
    {
        return GoalRepository::finalize($goalId);
    }

    /**
     * Reabre uma entrega
     *
     * @param int $goalId
     * @return RedirectResponse
     */
    public function reopen(int $goalId): RedirectResponse
    {
        return GoalRepository::reopen($goalId);
    }
}
