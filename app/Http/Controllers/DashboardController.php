<?php

namespace App\Http\Controllers;

use App\Events\ForecastUpdated;
use App\Repository\GoalRepository;
use App\Repository\PaymentForecastRepository;
use App\Repository\ProvisioningRepository;
use App\Repository\UserBalanceRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/***
 * Class DashboardController
 *
 * @package App\Http\Controllers
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class DashboardController extends Controller
{
    /**
     * Retorna a view de dashboard com os dados.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $userBalance = UserBalanceRepository::getCurrentBalance();

        $dashboardData = [
            'goals' => GoalRepository::getMonthGoal(),
            'payment_forecasts' => PaymentForecastRepository::getPaymentForecastMonth(),
            'cards_values' => [
                'entry' => $userBalance->user_forecast ?? 0,
                'goal_reach' => $userBalance->user_goal ?? 0,
                'balance' => $userBalance->user_balance ?? 0,
                'limit' => $userBalance->user_limit ?? 0
            ],
            'latest_entry' => ProvisioningRepository::getAll(10),
            'latest_forecasts' => PaymentForecastRepository::getAllCurrentMonth(10),
            'next_month_forecasts' => PaymentForecastRepository::getAllNextMonth(10)
        ];

        return view('dashboard', $dashboardData);
    }


    /**
     * Retorna valor disponível no balanço
     *
     * @return float
     */
    private function calculateBalance(): float
    {
        $income = auth()->user()->income;
        $forecastsNotPaid = PaymentForecastRepository::getPaymentForecastMonthPaid();

        return floatval($income) - floatval($forecastsNotPaid);
    }

    /**
     * Calcula o limite com base na renda - contas a vencer
     *
     * @return float
     */
    private function calculateLimit(): float
    {
        $income = auth()->user()->income;
        $forecastsNotPaid = PaymentForecastRepository::getPaymentForecastMonthNotPaid();

        return floatval($income) - floatval($forecastsNotPaid);
    }
}
