<?php

namespace App\Listeners;

use App\Events\ForecastUpdated;
use App\Repository\UserBalanceRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserBalanceUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ForecastUpdated  $event
     * @return void
     */
    public function handle(ForecastUpdated $event)
    {
        UserBalanceRepository::updateUserBalance($event);
    }

}
