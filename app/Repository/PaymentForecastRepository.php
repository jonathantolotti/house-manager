<?php


namespace App\Repository;

use App\Events\ForecastUpdated;
use App\Http\Requests\Forecasts\Pay;
use App\Http\Requests\Forecasts\Store;
use App\Http\Requests\Forecasts\Update;
use App\Models\PaymentForecast;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;

/**
 * Class PaymentForecastRepository
 *
 * @package App\Repository
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class PaymentForecastRepository
{

    /**
     * Retorna a previsão do mês
     *
     */
    public static function getPaymentForecastMonth()
    {
        return PaymentForecast::where('user_id', auth()->id())
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->get()
            ->toArray();
    }

    /**
     * Retorna previsão do mês paga
     *
     * @return mixed
     */
    public static function getPaymentForecastMonthPaid()
    {
        return PaymentForecast::where('user_id', auth()->id())
            ->where('paid', true)
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->sum('value');
    }


    /**
     * Retorna previsão do mês NÃO paga
     *
     * @return mixed
     */
    public static function getPaymentForecastMonthNotPaid()
    {
        return PaymentForecast::where('user_id', auth()->id())
            ->where('paid', false)
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->sum('value');
    }

    /**
     * Retorna previsão do mês NÃO paga do tipo EXPENSE
     *
     * @return mixed
     */
    public static function getPaymentForecastExpenseMonthNotPaid()
    {
        return PaymentForecast::where('user_id', auth()->id())
            ->where('paid', false)
            ->where('type', 'EXPENSE')
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->sum('value');
    }

    /**
     * Retorna previsão do mês NÃO paga do tipo EXPENSE
     *
     * @return mixed
     */
    public static function getPaymentForecastExpenseMonthPaid()
    {
        return PaymentForecast::where('user_id', auth()->id())
            ->where('paid', true)
            ->where('type', 'EXPENSE')
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->sum('value');
    }

    /**
     * Retorna toda a previsão do tipo EXPENSE
     *
     * @return mixed
     */
    public static function getPaymentForecastExpenseMonth()
    {
        return PaymentForecast::where('user_id', auth()->id())
            ->where('type', 'EXPENSE')
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->sum('value');
    }

    /**
     * Retorna toda a previsão do tipo INCOME
     *
     * @return mixed
     */
    public static function getPaymentForecastIncomeMonth()
    {
        return PaymentForecast::where('user_id', auth()->id())
            ->where('type', 'INCOME')
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->sum('value');
    }

    /**
     * Retorna previsão do mês NÃO paga do tipo EXPENSE
     *
     * @return mixed
     */
    public static function getPaymentForecastIncomeMonthPaid()
    {
        return PaymentForecast::where('user_id', auth()->id())
            ->where('paid', true)
            ->where('type', 'INCOME')
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->sum('value');
    }

    /**
     * Retorna todos os dados da previsão
     *
     * @param int|null $limit
     * @return mixed
     */
    public static function getAll(int $limit = null)
    {
        return PaymentForecast::select('payment_forecasts.id', 'due_date', 'payment_date', 'link_invoice', 'paid', 'value', 'payment_forecasts.created_at', 'name', 'type')
            ->where('user_id', auth()->id())
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->orderBy('due_date', 'asc')
            ->get()
            ->take($limit)
            ->toArray();
    }

    /**
     * Retorna todos os dados da previsãodo mês vigente
     *
     * @param int|null $limit
     * @return mixed
     */
    public static function getAllCurrentMonth(int $limit = null)
    {
        return PaymentForecast::select('payment_forecasts.id', 'due_date', 'payment_date', 'link_invoice', 'paid', 'value', 'payment_forecasts.created_at', 'name', 'type')
            ->where('user_id', auth()->id())
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->orderBy('due_date', 'asc')
            ->get()
            ->take($limit)
            ->toArray();
    }

    /**
     * Retorna todos os dados da previsãodo próximo mês
     *
     * @param int|null $limit
     * @return mixed
     */
    public static function getAllNextMonth(int $limit = null)
    {
        return PaymentForecast::select('payment_forecasts.id', 'due_date', 'payment_date', 'link_invoice', 'paid', 'value', 'payment_forecasts.created_at', 'name', 'type')
            ->where('user_id', auth()->id())
            ->whereMonth('due_date', date('m', strtotime("now +1month")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->orderBy('due_date', 'asc')
            ->get()
            ->take($limit)
            ->toArray();
    }

    /**
     * Efetua o pagamento de uma previsão
     *
     * @param Pay $request
     * @return mixed
     */
    public static function pay(Pay $request)
    {
        $updated = PaymentForecast::where('payment_forecasts.id', $request->forecastId)
                                    ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
                                    ->where('user_id', $request->user_id)
                                    ->update([
                                        'paid' => 1,
                                        'payment_date' => date("Y-m-d H:i:s", strtotime("now"))
                                    ]);

        ForecastUpdated::dispatch($request);

        return $updated;
    }

    /**
     * Retorna se a provisão já foi lançada no mês vigente.
     *
     * @param int $provisioningId
     * @return mixed
     */
    public static function hasForecast(int $provisioningId)
    {
        return PaymentForecast::where('provisioning_id', $provisioningId)
            ->whereMonth('due_date', date('m', strtotime("now")))
            ->whereYear('due_date', date('Y', strtotime("now")))
            ->count();
    }

    /**
     * Cria uma nova previsão
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public static function store(Store $request): RedirectResponse
    {
        $file = null;

        if ($request->file('invoice')) {
            $file = $request->file('invoice')->store('public/invoices');
        }

        $forecastCreated = PaymentForecast::create([
            'provisioning_id' => $request->provisioning,
            'due_date' => $request->due_date,
            'payment_date' => null,
            'value' => $request->value,
            'link_invoice' => $file,
            'paid' => 0
        ]);

        ForecastUpdated::dispatch($request);

        if ($forecastCreated) {
            return redirect()
                ->route('provisioning.index')
                ->with('success', 'Previsão cadastrada com sucesso!');
        }

        return redirect()
            ->route('provisioning.index')
            ->with('error', 'Erro ao cadastrar a previsão!');
    }

    /**
     * Retorna dados para edição da previsão
     *
     * @param int $forecastId
     * @return mixed
     */
    public static function edit(int $forecastId)
    {
        return PaymentForecast::select('payment_forecasts.id', 'due_date', 'payment_date', 'link_invoice', 'paid', 'value', 'payment_forecasts.created_at', 'name', 'type')
                ->where('payment_forecasts.id', $forecastId)
                ->where('payment_forecasts.id', $forecastId)
                ->where('user_id', auth()->id())
                ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
                ->first();
    }

    /**
     * Altera uma previsão
     *
     * @param Update $request
     * @return RedirectResponse
     */
    public static function update(Update $request): RedirectResponse
    {
        $file = null;

        if ($request->file('invoice')) {
            $file = $request->file('invoice')->store('public/invoices');
        }

        $forecastUpdated = PaymentForecast::where('id', $request->id)
                                ->update([
                                    'due_date' => $request->due_date,
                                    'value' => $request->value,
                                    'link_invoice' => $file
                                ]);

        ForecastUpdated::dispatch($request);

        if ($forecastUpdated) {
            return redirect()
                ->route('payment.forecast.index')
                ->with('success', 'Previsão alterada com sucesso!');
        }

        return redirect()
            ->route('payment.forecast.index')
            ->with('error', 'Erro ao alterar a previsão!');
    }

    public static function getForecastForFirstBalance(int $provisioning, string $dueDate, int $value)
    {
        return PaymentForecast::select('payment_forecasts.id')
            ->where('provisioning_id', $provisioning)
            ->where('due_date', $dueDate)
            ->where('value', $value)
            ->where('user_id', auth()->id())
            ->leftJoin('provisionings', 'provisionings.id', 'provisioning_id')
            ->first();
    }
}
