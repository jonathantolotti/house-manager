<?php

namespace App\Repository;

use App\Events\ForecastUpdated;
use App\Http\Requests\Base;
use App\Models\UserBalance;
use DivisionByZeroError;

/***
 * Class UserBalanceRepository
 *
 * @package App\Repository
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class UserBalanceRepository
{
    /**
     * Retorna o balanço geral do mês vigente.
     *
     * @return mixed
     */
    public static function getCurrentBalance()
    {
        return UserBalance::select()
            ->where('user_id', auth()->id())
            ->whereMonth('start_date', date('m', strtotime("now")))
            ->whereYear('start_date', date('Y', strtotime("now")))
            ->first();
    }

    /**
     * Método usado no observer de criação/atualização de uma previsão
     *
     * @param ForecastUpdated $request
     */
    public static function updateUserBalance(ForecastUpdated $request)
    {
        $currentBalance = self::getCurrentBalance();

        $currentBalance ? self::update($request->request) : self::create($request->request);
    }

    /**
     * Atualiza um balanço já existente
     *
     * @param Base $request
     */
    private static function update(Base $request)
    {
        $forecasts = PaymentForecastRepository::getPaymentForecastExpenseMonthNotPaid();

        // Valor em caixa: Renda + previsão INCOME - previsão EXPENSE, ambas PAGAS
        $balance = auth()->user()->income + (
                PaymentForecastRepository::getPaymentForecastIncomeMonthPaid() - PaymentForecastRepository::getPaymentForecastExpenseMonthPaid()
            );

        $currentGoal = GoalRepository::getCurrentGoal()->value ?? 0;

        // Meta atingida: Valor em caixa / meta
        try {
            $totalMonth = auth()->user()->income + PaymentForecastRepository::getPaymentForecastIncomeMonth() - PaymentForecastRepository::getPaymentForecastExpenseMonth();
            $goal = ($totalMonth / $currentGoal) * 100;
        } catch (DivisionByZeroError $e) {
            $goal = 0;
        }

        // Limite: Renda - meta - previsão EXPENSE a vencer
        $limit = $balance - $currentGoal - PaymentForecastRepository::getPaymentForecastExpenseMonthNotPaid();

        UserBalance::where('user_id', auth()->id())
            ->whereMonth('start_date', date('m', strtotime("now")))
            ->whereYear('start_date', date('Y', strtotime("now")))
            ->update([
            'user_forecast' => $forecasts,
            'user_goal' => $goal,
            'user_balance' => $balance,
            'user_limit' => $limit,
            'start_date' => date("Y-m-d", strtotime("now")),
            'user_id' => auth()->id()
        ]);
    }

    /**
     * Cria um novo balanço
     *
     * @param Base $request
     */
    private static function create(Base $request)
    {
        // A vencer: Previsão não paga do tipo EXPENSE

        $forecasts = PaymentForecastRepository::getPaymentForecastExpenseMonthNotPaid();

        // Valor em caixa: Renda + previsão INCOME - previsão EXPENSE, ambas PAGAS
        $balance = auth()->user()->income + (
            PaymentForecastRepository::getPaymentForecastIncomeMonthPaid() - PaymentForecastRepository::getPaymentForecastExpenseMonthPaid()
            );

        // Meta atingida: Valor em caixa / meta
        try {
            $totalMonth = auth()->user()->income + PaymentForecastRepository::getPaymentForecastIncomeMonth() - PaymentForecastRepository::getPaymentForecastExpenseMonth();
            $goal = ($totalMonth / GoalRepository::getCurrentGoal()->value) * 100;
        } catch (DivisionByZeroError $e) {
            $goal = 0;
        }

        // Limite: Balanço - Meta - previsão EXPENSE não paga
        $limit = $balance - GoalRepository::getCurrentGoal()->value - PaymentForecastRepository::getPaymentForecastExpenseMonthNotPaid();

        UserBalance::create([
            'user_forecast' => $forecasts,
            'user_goal' => $goal,
            'user_balance' => $balance,
            'user_limit' => $limit,
            'start_date' => date("Y-m-d", strtotime("now")),
            'user_id' => auth()->id()
        ]);
    }
}
