<?php

namespace App\Repository;

use App\Events\ForecastUpdated;
use App\Http\Requests\Auth\Store;
use App\Http\Requests\Auth\Update;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;

/***
 * Class UserRepository
 *
 * @package App\Repository
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class UserRepository
{
    /**
     * Persistência de dados na tabela de usuários.
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public static function store(Store $request): RedirectResponse
    {
        $newUser = (new User())->fill($request->toArray());

        if ($newUser->save()) {
            return redirect()
                ->route('users.new')
                ->with('success', 'Novo usuário cadastrado');
        }

        return redirect()
            ->route('users.new')
            ->with('error', 'Não foi possível cadastrar o usuário');
    }

    /**
     * Atualiza as informações de um usuário.
     *
     * @param Update $update
     * @return RedirectResponse
     */
    public static function update(Update $update): RedirectResponse
    {
        $userUpdated = User::find(auth()->id())->fill($update->toArray());

        ForecastUpdated::dispatch($update);

        if ($userUpdated->save()) {
            return redirect()
                ->route('users.edit')
                ->with('success', 'Dados alterados com sucesso.');
        }

        return redirect()
            ->route('users')
            ->with('error', 'Não foi possível alterar os dados.');
    }
}
