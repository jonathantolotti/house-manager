<?php


namespace App\Repository;

use App\Http\Requests\Provisioning\Store;
use App\Models\Provisioning;
use Illuminate\Http\RedirectResponse;

/***
 * Class ProvisioningRepository
 *
 * @package App\Repository
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class ProvisioningRepository
{
    /**
     * Retorna todas as contas cadastradas.
     *
     * @param int|null $limit
     * @return mixed
     */
    public static function getAll(int $limit = null)
    {
        $provisioning = Provisioning::where('user_id', auth()->id())
                            ->orderBy('date', 'asc')
                            ->get()
                            ->take($limit)
                            ->toArray();

        foreach ($provisioning as $key => $value) {
            $provisioning[$key]['date'] = date('d', strtotime($value['date']));
            $provisioning[$key]['type'] = \App\Enum\Provisioning::getType($value['type']);
            $provisioning[$key]['has_forecast'] = PaymentForecastRepository::hasForecast($value['id']);
        }

        return $provisioning;
    }

    /**
     * Cria uma nova provisão
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public static function store(Store $request): RedirectResponse
    {
        $provisioning = (new Provisioning())->fill($request->toArray());

        if ($provisioning->save()) {
            return redirect()
                ->route('provisioning.index')
                ->with('success', 'Nova conta cadastrada.');
        }

        return redirect()
            ->route('provisioning.index')
            ->with('error', 'Erro ao cadastrar a conta.');
    }

    /**
     * Retorna dados para edição de uma provisão
     *
     * @param int $id
     * @return mixed
     */
    public static function edit(int $id)
    {
        $data = Provisioning::where('user_id', auth()->id())
            ->where('id', $id)
            ->first();

        $data->date = date('Y-m')."-".date('d', strtotime($data->date));

        return $data;
    }

    /**
     * Altera dados de uma previsão
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public static function update(Store $request): RedirectResponse
    {
        $provisioningUpdated = Provisioning::find($request->id)->fill($request->toArray());

        if ($provisioningUpdated->save()) {
            return redirect()
                ->route('provisioning.index')
                ->with('success', 'Dados alterados com sucesso.');
        }

        return redirect()
            ->route('provisioning.index')
            ->with('error', 'Não foi possível alterar os dados.');
    }
}
