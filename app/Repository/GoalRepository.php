<?php


namespace App\Repository;

use App\Events\ForecastUpdated;
use App\Http\Requests\Goals\Store;
use App\Models\UserGoal;
use Illuminate\Http\RedirectResponse;

/***
 * Class GoalRepository
 *
 * @package App\Repository
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class GoalRepository
{
    /**
     * Retorna as informações da meta do mês atual.
     *
     * @return mixed
     */
    public static function getMonthGoal()
    {
        return UserGoal::where('user_id', auth()->id())
            ->whereMonth('start_date', date('m', strtotime("now")))
            ->whereYear('start_date', date('Y', strtotime("now")))
            ->first();
    }

    /**
     * Retorna dados da meta mais atualizada
     *
     * @return mixed
     */
    public static function getCurrentGoal()
    {
        return UserGoal::where('user_id', auth()->id())
            ->where('reach', 0)
            ->whereMonth('start_date', date('m', strtotime("-1 months")))
            ->whereYear('start_date', date('Y', strtotime("now")))
            ->first();
    }

    /**
     * Retorna todas as metas
     *
     * @return mixed
     */
    public static function getAllGoals()
    {
        $goals = UserGoal::where('user_id', auth()->id())
            ->orderBy('start_date', 'desc')
            ->get()
            ->toArray();

        foreach ($goals as $key => $value) {
            $goals[$key]['start_date'] = date('m-Y', strtotime($value['start_date']));
        }

        return $goals;
    }

    /**
     * Cria uma nova meta
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public static function createGoal(Store $request): RedirectResponse
    {
        $checkGoal = UserGoal::where('reach', 0)
            ->whereMonth('start_date', date('m', strtotime($request->start_date)))
            ->whereYear('start_date', date('Y', strtotime($request->start_date)))
            ->count();

        if ($checkGoal) {
            return redirect()
                ->route('goals.index')
                ->with('error', 'Você já possui uma meta cadastrada neste mês ou não atingida.');
        }

        $newGoal = (new UserGoal())->fill($request->toArray());

        ForecastUpdated::dispatch($request);

        if ($newGoal->save()) {
            return redirect()
                ->route('goals.index')
                ->with('success', 'Nova meta cadastrada.');
        }

        return redirect()
            ->route('goals.index')
            ->with('error', 'Não foi possível cadastrar a meta.');
    }

    /**
     * Retorna dados de uma meta para edição
     *
     * @param int $goalId
     * @return mixed
     */
    public static function getGoal(int $goalId)
    {
        return UserGoal::where('id', $goalId)
            ->where('user_id', auth()->id())
            ->first();
    }

    /**
     * Altera dados de uma meta
     *
     * @param Store $request
     * @return RedirectResponse
     */
    public static function updateGoal(Store $request): RedirectResponse
    {
        $goalUpdated = UserGoal::find($request->id)->fill($request->toArray());

        ForecastUpdated::dispatch($request);

        if ($goalUpdated->save()) {
            return redirect()
                ->route('goals.index')
                ->with('success', 'Dados alterados com sucesso.');
        }

        return redirect()
            ->route('goals.index')
            ->with('error', 'Não foi possível alterar os dados.');
    }

    /**
     * Finaliza uma meta
     *
     * @param int $goalId
     * @return RedirectResponse
     */
    public static function finalize(int $goalId): RedirectResponse
    {
        $goal = UserGoal::where('user_id', auth()->id())
            ->where('id', $goalId)
            ->update(['reach' => 1]);

        if ($goal) {
            return redirect()
                ->route('goals.index')
                ->with('success', 'Meta finalizada com sucesso.');
        }

        return redirect()
            ->route('goals.index')
            ->with('error', 'Não foi possível alterar os dados.');
    }

    /**
     * Reabre uma meta
     *
     * @param int $goalId
     * @return RedirectResponse
     */
    public static function reopen(int $goalId): RedirectResponse
    {
        $goal = UserGoal::where('user_id', auth()->id())
            ->where('id', $goalId)
            ->update(['reach' => 0]);

        if ($goal) {
            return redirect()
                ->route('goals.index')
                ->with('success', 'Meta reaberta com sucesso.');
        }

        return redirect()
            ->route('goals.index')
            ->with('error', 'Não foi possível alterar os dados.');
    }
}
