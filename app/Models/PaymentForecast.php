<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentForecast extends Model
{
    use HasFactory;

    protected $fillable = [
        'provisioning_id',
        'due_date',
        'payment_date',
        'value',
        'link_invoice',
        'paid'
    ];
}
