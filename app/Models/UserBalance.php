<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_forecast',
        'user_goal',
        'user_balance',
        'user_limit',
        'start_date',
        'user_id'
    ];
}
