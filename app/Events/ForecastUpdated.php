<?php

namespace App\Events;

use App\Http\Requests\Base;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ForecastUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Base $request;

    /**
     * Create a new event instance.
     *
     * @param Base $request
     */
    public function __construct(Base $request)
    {
        $this->request = $request;
    }

}
