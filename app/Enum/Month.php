<?php


namespace App\Enum;

/***
 * Class Month
 *
 * @package App\Enum
 *
 * @author Jonathan S. Tolotti <jonathanstolotti@gmail.com>
 */

class Month
{
    private const MONTH = [
        1 => 'Janeiro',
        2 => 'Fevereiro',
        3 => 'Março',
        4 => 'Abril',
        5 => 'Maio',
        6 => 'Junho',
        7 => 'Julho',
        8 => 'Agosto',
        9 => 'Setembro',
        10 => 'Outubro',
        11 => 'Novembro',
        12 => 'Dezembro',
    ];

    /**
     * Retorna o mês.
     *
     * @param int $month
     * @return string
     */
    public static function getMonth(int $month): string
    {
        return self::MONTH[$month];
    }
}
