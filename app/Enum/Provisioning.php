<?php


namespace App\Enum;


class Provisioning
{
    private const TYPE = [
        'INCOME' => 'Receita',
        'EXPENSE' => 'Despesa'
    ];

    /**
     * Retorna o Enum de tipo de provisão.
     *
     * @param string $type
     * @return string
     */
    public static function getType(string $type): string
    {
        return self::TYPE[$type];
    }
}
